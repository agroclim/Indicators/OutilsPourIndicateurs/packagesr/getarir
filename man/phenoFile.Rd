% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/phenoFile.R
\docType{data}
\name{phenoFile}
\alias{phenoFile}
\title{Pheno file}
\format{
A tibble: 6 × 10
\describe{
\item{cell}{cell}
\item{year}{year of stade}
\item{s0}{stade0}
\item{s1}{stade1}
\item{s2}{stade2}
\item{s3m10s3p10}{stades3m10s3p10 }
\item{s3}{stades3}
\item{s4}{stades4}
\item{s0m15s1}{stades0m15s1 }
\item{s5}{stade5}

...
}
}
\source{
renan LE roux
}
\usage{
phenoFile
}
\description{
Pheno file
}
\keyword{datasets}
